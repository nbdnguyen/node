var http = require('http');
const readline = require('readline');
const fs = require("fs");
const os = require("os");

const menu = () => {
    return fs.readFileSync("menu.txt", "utf-8", (err, content) => {
        if(err) {
            return console.log(err);
        }
        return content;
    });
}

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const ask = () => {
    rl.question("Type a number: ", number => {
        pick(number);
        //rl.close();
    });
}

const pick = (number) => {
    switch(number) {
        case "1":
            console.log("Reading package.json file...");
            console.log(fs.readFileSync("package.json", "utf-8"));
            ask();
            break;
        case "2":
            console.log("Getting OS info...");
            console.log("SYSTEM MEMORY:" + (os.totalmem()/1024/1024/1024).toFixed(2) + " GB");
            console.log("FREE MEMORY: " + (os.freemem()/1024/1024/1024).toFixed(2) + " GB");
            console.log("CPU CORES: " + os.cpus().length);
            console.log("ARCH: " + os.arch());
            console.log("PLATFORM: " + os.platform());
            console.log("RELEASE: " + os.release());
            console.log("USER: " + os.userInfo());
            ask();
            break;
        case "3":
            console.log("Starting HTTP server...");
            console.log("Listening on port 3000...");
            http.createServer( (req,res) => {
                res.write("Hello World!");
                res.end();
            }).listen(3000);
            break;
        default:
            console.log("Invalid option.");
            ask();
            break;
    }
}

console.log(menu());
ask();