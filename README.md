# Node

Includes:

* **NodeJS Console App**: Gets a menu with options in the terminal. 
    1. Prints the package.json file.
    2. Prints some OS info.
    3. Opens a HTTP server on port 3000, browser will display "Hello World!".
    
* **NodeJS Web Scraper**: Opens poe.ninja/challenge/currency and saves item name, and current stat numbers to a .json file.
