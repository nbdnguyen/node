const puppeteer = require("puppeteer");
const fs = require("fs");

(async () => {
    const selector = "#main > div > div:nth-child(3) > table > tbody > tr";
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto("https://poe.ninja/challenge/currency");
    await page.waitForSelector(selector);

    let content = await page.evaluate(result => {
        const elements = Array.from(document.querySelectorAll(result));
        return elements.map(element => {
            let resultObject = {}
            resultObject.name = element.children[0].textContent;

            resultObject.buyPercent = element.children[1].textContent;
            resultObject.buyPay = element.children[2].textContent;
            resultObject.buyGet = element.children[3].textContent;

            if(element.children.length > 7) {
                resultObject.sellPercent = element.children[5].textContent;
                resultObject.sellPay = element.children[6].textContent;
                resultObject.sellGet = element.children[7].textContent;
            }

            return resultObject;
        })
    }, selector)
    fs.writeFile("PoEcurrency.json", JSON.stringify(content, null, " "), function(err) {
        if(err) {
            console.log(err);
        }
    });
    await browser.close();

})()